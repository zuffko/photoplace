//
//  PlacePFObjectSubclass.h
//  PhotoPlace
//
//  Created by zufa on 15/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Parse/Parse.h>

@interface Place : PFObject <PFSubclassing>
+ (NSString *)parseClassName;

@property (retain) NSString *placeTitle;
@property (retain) NSString *placeDescription;
@property (retain) NSString *placeAuthorDisplayName;
@property (retain) NSString *placeLatitude;
@property (retain) NSString *placeLongitude;
@property (retain) NSString *placeCreateDate;
@property (retain) PFUser *placAauthorPoiter;
@property (retain) PFFile *placePhoto;
@end
