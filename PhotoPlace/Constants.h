//Place
extern NSString *const placeClassNameKey;
extern NSString *const placeAuthorDisplayNameKey;
extern NSString *const placeTitleKey;
extern NSString *const placeDescriptionKey;
extern NSString *const placeLatitudeKey;
extern NSString *const placeLongitudeKey;
extern NSString *const placAauthorPoiterKey;
extern NSString *const placeCreateDateKey;
//User
extern NSString *const userDisplayNameKey;
extern NSString *const userFacebookIdKey;
extern NSString *const userAvatarKey;
//Comment
extern NSString *const commentAuthorDisplayNameKey;
extern NSString *const commentClassNameKey;
extern NSString *const commentTextKey;
extern NSString *const commentAuthorFacebookIdKey;
extern NSString *const commentBelongsPointerKey;
extern NSString *const commentCreateDateKey;
//Like
extern NSString *const likeClassNameKey;
extern NSString *const likeAuthorFacebookIdKey;
extern NSString *const likeTextKey;
extern NSString *const likePlacePointerKey;