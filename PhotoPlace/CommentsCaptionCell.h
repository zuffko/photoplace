//
//  CommentsCaptionCell.h
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsCaptionCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UILabel *commentsCaption;

+(CommentsCaptionCell*)cell;
+(NSString *)cellID;
@end
