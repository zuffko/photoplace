//
//  facebookFriendPickerViewController.h
//  PhotoPlace
//
//  Created by zufa on 10/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>

@interface FacebookFriendPickerViewController : FBFriendPickerViewController <FBFriendPickerDelegate>

@end
