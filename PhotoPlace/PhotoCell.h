//
//  PhotoCell.h
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UIImageView *placePhoto;
@property (nonatomic, retain) IBOutlet UILabel *likesCount;
+(PhotoCell*)cell;
+(NSString *)cellID;
@end
