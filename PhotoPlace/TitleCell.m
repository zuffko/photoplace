//
//  TitleCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//
#import "PlaceDetailTableViewController.h"
#import "TitleCell.h"
#import "Design.h"


@implementation TitleCell
@synthesize titleTextView=_titleTextView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }


    return self;
}
-(void)awakeFromNib
{

    self.backgroundView = [Design setClearBackView];
    
    _titleTextView=[[UITextView alloc] initWithFrame:CGRectMake(5, 10, 310, 10)];
    _titleTextView.font = [Design setFontLobsterWithSize:20];
    _titleTextView.backgroundColor = [UIColor clearColor];
    _titleTextView.textAlignment = NSTextAlignmentCenter;
    _titleTextView.scrollEnabled = NO;
    _titleTextView.textColor=[Design setColor];
    _titleTextView.editable=NO;

    [self.contentView addSubview:_titleTextView];
}

+(TitleCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"titleCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"titleCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
