
#import <UIKit/UIKit.h>
#import "ProtocolQueries.h"
#import "Place.h"

@interface PlaceDetailTableViewController : UITableViewController <UITextFieldDelegate, ProtocolQueries>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (strong, nonatomic) id placeDetailItem;
@property (nonatomic,retain) IBOutlet UITextField *commentTextField;
@property (nonatomic, retain) NSArray *comments;
@property (nonatomic, retain) NSArray *likes;
@property (nonatomic, retain) ProtocolQueries *data;
@property (nonatomic, strong) Place *loadplace;
@property (nonatomic, retain) NSMutableArray *cells;
@property int likesCount;

- (IBAction)addLike:(id)sender;

@end
