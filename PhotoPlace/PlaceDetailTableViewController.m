#import "PlaceDetailTableViewController.h"
#import <Parse/Parse.h>
#import "PlaceTableViewController.h"
#import "Place.h"
#import "QuartzCore/CALayer.h"
#import <MapKit/MapKit.h>
#import "TitleCell.h"
#import "PhotoCell.h"
#import "AuthorAndDateCell.h"
#import "DescriptionCaptionCell.h"
#import "DescriptionCell.h"
#import "CommentsCaptionCell.h"
#import "CommentsCell.h"
#import "AddCommentCell.h"
#import "Constants.h"
#import "Like.h"
#import "Comment.h"
#import "Place.h"

@interface PlaceDetailTableViewController ()

@end

@implementation PlaceDetailTableViewController
@synthesize comments=_comments;
@synthesize likes=_likes;
@synthesize tableView=_tableView;
@synthesize data=_data;
@synthesize commentTextField=_commentTextField;
@synthesize loadplace=_loadplace;
@synthesize placeDetailItem = _placeDetailItem;
@synthesize cells=_cells;
@synthesize likesCount=_likesCount;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:
                                     [UIImage imageNamed:@"DetailTVC_background.png"]];
    _data =[ProtocolQueries new] ;
    _data.delegate = self;
    [_data getLikes:_loadplace.objectId cashePolicy:kPFCachePolicyCacheElseNetwork];
    [_data getComments:_loadplace.objectId cashePolicy:kPFCachePolicyCacheElseNetwork];
}

-(void)ShowHUD
{
    [MBProgressHUD showHUDAddedTo:_tableView animated:YES];
}

-(void)hideHud
{
    [MBProgressHUD hideAllHUDsForView:_tableView animated:YES];
    [_tableView reloadData];
}

-(void)loadLikes:(NSNumber*)likes_count
{
    _likesCount=[(NSNumber*)likes_count intValue];
    [self loadCell];
    [self hideHud];
}

-(void)loadComments:(NSArray*)comments
{
    _comments =(NSArray*)comments;
    [_comments retain];
    [self hideHud];
    [self loadCell];
}


-(void)loadCell
{
    _cells = [[NSMutableArray alloc] init];
    TitleCell *titleCell;
    titleCell = [_tableView dequeueReusableCellWithIdentifier:[TitleCell cellID]];
    if (!titleCell) {
        titleCell=[[[TitleCell cell]retain] autorelease];
    }
    CGSize stringSize = [_loadplace.placeTitle sizeWithFont:[UIFont fontWithName:@"Lobster" size:20] constrainedToSize:CGSizeMake(295, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect textSizeTrame =CGRectMake(5, 10, 310, (stringSize.height+15));
    
    titleCell.titleTextView.frame =textSizeTrame;
    titleCell.titleTextView.text=_loadplace.placeTitle;
    [_cells addObject:titleCell];
    
    PhotoCell *photoCell;
    photoCell = [_tableView dequeueReusableCellWithIdentifier:[PhotoCell cellID]];
    if (!photoCell) {
        photoCell=[[[PhotoCell cell]retain] autorelease];
    }
    PFFile *file = _loadplace.placePhoto;
    NSData *data = [file getData];
    photoCell.placePhoto.image=[UIImage imageWithData:data];;
    if (_likesCount>0) {
        photoCell.likesCount.text =[[NSString alloc] initWithFormat:@"%d", _likesCount ];
    }
    [_cells addObject:photoCell];
    
    AuthorAndDateCell *authorAndDateCell;
    authorAndDateCell = [_tableView dequeueReusableCellWithIdentifier:[AuthorAndDateCell cellID]];
    if (!authorAndDateCell) {
        authorAndDateCell=[[[AuthorAndDateCell cell]retain] autorelease];
    }
    authorAndDateCell.authorName.text = _loadplace.placeAuthorDisplayName;
    authorAndDateCell.creationDate.text = _loadplace.placeCreateDate;
    [_cells addObject:authorAndDateCell];
    
    DescriptionCaptionCell *descriptionCaptionCell;
    descriptionCaptionCell = [_tableView dequeueReusableCellWithIdentifier:[DescriptionCaptionCell cellID]];
    if (!descriptionCaptionCell) {
        descriptionCaptionCell=[[[DescriptionCaptionCell cell]retain] autorelease];
    }
    [_cells addObject:descriptionCaptionCell];
    
    DescriptionCell *descriptionCell;
    descriptionCell = [_tableView dequeueReusableCellWithIdentifier:[DescriptionCell cellID]];
    if (!descriptionCell) {
        descriptionCell=[[[DescriptionCell cell]retain] autorelease];
    }
    
    CGSize stringSizeDescription = [_loadplace.placeDescription sizeWithFont:[UIFont fontWithName:@"LiberationSerif-Italic" size:15] constrainedToSize:CGSizeMake(295, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect descriptionSizeFrame =CGRectMake(5, 0, 310, (stringSizeDescription.height+15));
    
    descriptionCell.descriptionTextView.frame =descriptionSizeFrame;
    descriptionCell.descriptionTextView.text=_loadplace.placeDescription;
    
    if ([_loadplace.placeDescription length]<40) {
        descriptionCell.descriptionTextView.textAlignment = NSTextAlignmentCenter;
    }else{
        descriptionCell.descriptionTextView.textAlignment = NSTextAlignmentJustified;
    }
    [_cells addObject:descriptionCell];
    
    
    
    CommentsCaptionCell *commentsCaptionCell;
    commentsCaptionCell = [_tableView dequeueReusableCellWithIdentifier:[CommentsCaptionCell cellID]];
    if (!commentsCaptionCell) {
        commentsCaptionCell=[[[CommentsCaptionCell cell]retain] autorelease];
    }
    [_cells addObject:commentsCaptionCell];
    
    AddCommentCell *addCommentCell;
    addCommentCell = [_tableView dequeueReusableCellWithIdentifier:[AddCommentCell cellID]];
    if (!addCommentCell) {
        addCommentCell=[[[AddCommentCell cell]retain] autorelease];
    }
    addCommentCell.commentTextField.delegate = self;
    [_cells addObject:addCommentCell];
    
    if ([_comments count]>0) {
        int i = 0;
        while (i<[_comments count]) {
            
            CommentsCell *cell;
            cell = [_tableView dequeueReusableCellWithIdentifier:[CommentsCell cellID]];
            if (!cell) {
                cell=[[[CommentsCell cell]retain] autorelease];
            }
            
            cell.commentText.text = [[_comments objectAtIndex:i]objectForKey:commentTextKey];
            
            cell.commentsAuthorName.text = [[_comments objectAtIndex:i]objectForKey:commentAuthorDisplayNameKey];
            
            cell.commentsDate.text = [[_comments objectAtIndex:i]objectForKey:commentCreateDateKey];
            
            PFFile *avatarFile = [[PFUser currentUser] objectForKey:userAvatarKey];
            [avatarFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    UIImage *pickImage = [UIImage imageWithData:data];
                    cell.userAvatar.image = pickImage;
                }
            }];
            i++;
            [_cells addObject:cell];
        }
        
    }
[_tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ([_cells count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
        return [_cells objectAtIndex:indexPath.row];

    [tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row ==0) {
        CGSize stringSize = [_loadplace.placeTitle sizeWithFont:[UIFont fontWithName:@"Lobster" size:20]
                                   constrainedToSize:CGSizeMake(295, 9999)
                                       lineBreakMode:NSLineBreakByWordWrapping];
        return stringSize.height+13;
    }else if (indexPath.row ==1){
        return 320;
    }else if (indexPath. row == 2){
        return 24;
    }else if (indexPath.row ==3){
        return 25;
    }else if (indexPath.row ==4){
        CGSize stringSize = [_loadplace.placeDescription sizeWithFont:[UIFont fontWithName:@"LiberationSerif-Italic" size:15]
                                         constrainedToSize:CGSizeMake(295, 9999)
                                             lineBreakMode:NSLineBreakByWordWrapping];
        return stringSize.height+13;
    }else if (indexPath.row == 5){
        return 31;
    }else if (indexPath.row == 6){
        return 63;
    }else{
        
        return 50;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    _commentTextField = (UITextField *) [self.view viewWithTag:130];
    if (![PFUser currentUser] && ![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]])
    {
        UIAlertView* alertLogin = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                             message:@"You not logged in! :("
                                                            delegate:self
                                                   cancelButtonTitle:@"login"
                                                   otherButtonTitles:@"Cancel", nil];
        [alertLogin show];
        [alertLogin release];
    }else{
        if ([_commentTextField.text length]<1) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Add comment"
                                                            message:@"You have not entered a comment!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"I enter"
                                                  otherButtonTitles: nil];
            [alert show];
            [alert release];

        }else{
        NSDate *dateToday =[NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeStyle:NSDateFormatterShortStyle];
        [format setDateStyle:NSDateFormatterMediumStyle];
        NSString *string = [format stringFromDate:dateToday];
        [format release];
            

            Comment *addComment = [Comment object];
            addComment.commentText= _commentTextField.text;
            addComment.commentCreateDate = string;
            addComment.commentAuthorDisplayName = [[PFUser currentUser] objectForKey:userDisplayNameKey];
            addComment.commentAuthorFacebookId = [[PFUser currentUser] objectForKey:userFacebookIdKey];
            addComment.commentBelongsPointer = [PFObject objectWithoutDataWithClassName:placeClassNameKey objectId:_loadplace.objectId];
            [addComment saveInBackground];
            
        [_commentTextField resignFirstResponder];
        _commentTextField.text = nil;
        [_data getComments:_loadplace.objectId cashePolicy:kPFCachePolicyNetworkElseCache];
        [_comments retain];
        [_tableView reloadData];
        }
    }
    return YES;
}

- (IBAction)addLike:(id)sender
{
    if (![PFUser currentUser] && ![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]])
    {
        UIAlertView* alertLogin = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                             message:@"You not logged in! :("
                                                            delegate:self
                                                   cancelButtonTitle:@"login"
                                                   otherButtonTitles:@"Cancel", nil];
        [alertLogin show];
        [alertLogin release];
    }else{
    
    if ([_data secondLike:_loadplace.objectId]==NO) {
        Like *myLike = [Like object];
        myLike.likeText = @"like";
        myLike.likeAuthorFacebookId = [[PFUser currentUser] objectForKey:userFacebookIdKey];
        myLike.likePlacePointer= [PFObject objectWithoutDataWithClassName:placeClassNameKey objectId:_loadplace.objectId];
        
        [myLike save];
        [_data getLikes:_loadplace.objectId cashePolicy:kPFCachePolicyNetworkElseCache];
        [_likes retain];
        [_tableView reloadData];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Action"
                                                        message:@"Like you have already put!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    [_tableView reloadData];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0){
        [self performSegueWithIdentifier:@"loginSegue" sender:nil];
    }
}

-(IBAction)ShowMap:(id)sender
{
    MKPlacemark *myPlacemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake([_loadplace.placeLatitude floatValue], [_loadplace.placeLongitude floatValue]) addressDictionary:nil];
    MKMapItem *myPoint = [[MKMapItem alloc] initWithPlacemark:myPlacemark] ;
    myPoint.name=_loadplace.placeTitle;
    NSDictionary *options = @{
                              MKLaunchOptionsMapTypeKey :[NSNumber numberWithInteger:MKMapTypeHybrid],
                              MKLaunchOptionsMapCenterKey :[NSValue valueWithMKCoordinate:CLLocationCoordinate2DMake([_loadplace.placeLatitude floatValue], [_loadplace.placeLongitude floatValue])],
                              
                              MKLaunchOptionsMapSpanKey :[NSValue valueWithMKCoordinateSpan:MKCoordinateSpanMake(1, 1)]};
    
    [myPoint openInMapsWithLaunchOptions:options];
    [myPlacemark autorelease];
    [myPoint autorelease];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_commentTextField) {
        [_commentTextField resignFirstResponder];
    }
    [self.tableView endEditing:YES];

}

@end
