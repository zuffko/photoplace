//
//  CommentsCaptionCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "CommentsCaptionCell.h"
#import "Design.h"

@implementation CommentsCaptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(void)awakeFromNib
{
    _commentsCaption.font = [Design setFontLiberationSerifBoldItalicWithSize:21];
    _commentsCaption.textColor=[Design setColor];
    _commentsCaption.text = @"Comments";
    self.backgroundView = [Design setClearBackView];
}


+(CommentsCaptionCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"сommentsCaptionCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"сommentsCaptionCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
