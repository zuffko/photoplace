
#import "Constants.h"


//Place
 NSString *const placeClassNameKey = @"Place";
 NSString *const placeAuthorDisplayNameKey = @"placeAuthorDisplayName";
 NSString *const placeTitleKey = @"placeTitle";
 NSString *const placeDescriptionKey = @"placeDescription";
 NSString *const placeLatitudeKey = @"placeLatitude";
 NSString *const placeLongitudeKey = @"placeLongitude";
 NSString *const placAauthorPoiterKey = @"placAauthorPoiter";
 NSString *const placeCreateDateKey = @"placeCreateDate";


//User
 NSString *const userDisplayNameKey = @"userDisplayName";
 NSString *const userFacebookIdKey = @"userFacebookId";
 NSString *const userAvatarKey = @"userAvatar";

//Comment
 NSString *const commentAuthorDisplayNameKey = @"commentAuthorDisplayName";
 NSString *const commentClassNameKey = @"Comments";
 NSString *const commentTextKey = @"commentText";
 NSString *const commentAuthorFacebookIdKey = @"commentAuthorFacebookId";
NSString *const commentBelongsPointerKey = @"commentBelongsPointer";
NSString *const commentCreateDateKey=@"commentCreateDate";

//Like
 NSString *const likeClassNameKey = @"Likes";
 NSString *const likeAuthorFacebookIdKey = @"likeAuthorFacebookId";
 NSString *const likeTextKey = @"like";
 NSString *const likePlacePointerKey = @"likePlacePointer";

//Fonts
