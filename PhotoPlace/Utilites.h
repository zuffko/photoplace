//
//  Utilites.h
//  PhotoPlace
//
//  Created by zufa on 17/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilites : NSObject
+ (UIImage*)imageCrop:(UIImage *)imageToCrop toRect:(CGRect)rect;
@end
