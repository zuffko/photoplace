//
//  DescriptionCaptionCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "DescriptionCaptionCell.h"
#import "Design.h"

@implementation DescriptionCaptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(void)awakeFromNib
{
    UIView *backView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    backView.backgroundColor = [UIColor clearColor];

    _descriptionCaption.font = [Design setFontLiberationSerifBoldItalicWithSize:21];
    _descriptionCaption.textColor=[Design setColor];
    _descriptionCaption.text = @"Description";
    
}


+(DescriptionCaptionCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"descriptionCaptionCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"descriptionCaptionCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
