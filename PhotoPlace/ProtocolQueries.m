#import "ProtocolQueries.h"
#import <Parse/Parse.h>
#import "Constants.h"
#import "PlaceDetailTableViewController.h"

@implementation ProtocolQueries 
@synthesize delegate;
@synthesize requestedPlaceData;
@synthesize photos;
@synthesize comments=_comments;
@synthesize likes=_likes;


- (void)getPlacesWithCashePolicy:(int)cashePolicy
{
    SEL selector = @selector(ShowHUD);
    [delegate performSelector:selector];
    __block ProtocolQueries *protocolQueries = self;
    PFQuery *query = [PFQuery queryWithClassName:placeClassNameKey];
    query.cachePolicy = (int)cashePolicy;
    [query orderByDescending: placeCreateDateKey];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        SEL selectorq = @selector(setPlases:);
        [protocolQueries.delegate performSelector:selectorq withObject:objects];
        
    }];
}


- (void)getComments:(NSString *)placeId cashePolicy:(int)cashePolicy;
{
    SEL selector = @selector(ShowHUD);
    [delegate performSelector:selector];
    __block ProtocolQueries *protocolQueries = self;
    PFQuery *queryComments = [PFQuery queryWithClassName:commentClassNameKey];
    queryComments.cachePolicy= (int)cashePolicy;
    [queryComments whereKey:commentBelongsPointerKey equalTo:[PFObject objectWithoutDataWithClassName:placeClassNameKey objectId:(NSString*)placeId]];
    [queryComments findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        SEL selectorq = @selector(loadComments:);
        [protocolQueries.delegate performSelector:selectorq withObject:objects];
    }];

}

-(void)getLikes:(NSString *)placeId cashePolicy:(int)cashePolicy
{
    SEL selector = @selector(ShowHUD);
    [delegate performSelector:selector];
    __block ProtocolQueries *protocolQueries = self;
    PFQuery *queryLikes = [PFQuery queryWithClassName:likeClassNameKey];
    queryLikes.cachePolicy= (int)cashePolicy;
    [queryLikes whereKey:likePlacePointerKey equalTo:[PFObject objectWithoutDataWithClassName:placeClassNameKey objectId:(NSString *)placeId]];
    [queryLikes countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            SEL selectorq = @selector(loadLikes:);
            [protocolQueries.delegate performSelector:selectorq withObject:[NSNumber numberWithInt:(int)number]];
        } else {
            
        }
    }];
}

- (BOOL)secondLike:(NSString *)placeId
{
    PFQuery * queryLikeForPlaceId = [PFQuery queryWithClassName:likeClassNameKey];
    [queryLikeForPlaceId whereKey:likePlacePointerKey equalTo:[PFObject objectWithoutDataWithClassName:placeClassNameKey objectId:(NSString *)placeId]];
    [queryLikeForPlaceId whereKey:likeAuthorFacebookIdKey equalTo:[[PFUser currentUser] objectForKey:userFacebookIdKey]];
    NSArray *qwe = [queryLikeForPlaceId findObjects];
    if ([qwe count]==0) {
        return NO;
    }else{
        return YES;
    }
    
}



@end
