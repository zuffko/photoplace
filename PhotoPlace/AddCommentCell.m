//
//  AddCommentCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "AddCommentCell.h"
#import "Design.h"

@implementation AddCommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    _addCommentCaption.font = [Design setFontLiberationSerifBoldItalicWithSize:17];
    _addCommentCaption.textColor=[Design setColor];
    _addCommentCaption.text = @"Add comment";
    
    self.backgroundView = [Design setClearBackView];

}


+(AddCommentCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"addCommentCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"addCommentCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
