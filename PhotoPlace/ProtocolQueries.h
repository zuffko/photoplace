
#import <Foundation/Foundation.h>
#import <MBProgressHUD/MBProgressHUD.h>

@protocol ProtocolQueries <NSObject>
@end

@interface ProtocolQueries : NSObject <MBProgressHUDDelegate> {
    id <ProtocolQueries> delegate;
}

@property (nonatomic, assign) id <ProtocolQueries> delegate;
@property (nonatomic,strong) NSArray *requestedPlaceData;
@property (nonatomic,retain) NSArray *photos;
@property (nonatomic,retain) NSArray *comments;
@property (nonatomic,retain) NSArray *likes;

- (void)getPlacesWithCashePolicy:(int)cashePolicy;
- (void)getComments:(NSString *)placeId cashePolicy:(int)cashePolicy;
- (void)getLikes:(NSString *)placeId cashePolicy:(int)cashePolicyy;
- (BOOL)secondLike:(NSString *)placeId;
@end
