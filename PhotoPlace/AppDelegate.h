//
//  AppDelegate.h
//  PhotoPlace
//
//  Created by zufa on 4/4/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "Place.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
