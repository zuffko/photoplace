//
//  AuthorAndDateCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "AuthorAndDateCell.h"
#import "Design.h"

@implementation AuthorAndDateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

-(void)awakeFromNib
{
    _authorName.font = [Design setFontLiberationSerifBoldItalicWithSize:17];
    _authorName.textColor=[Design setColor];
    
    _creationDate.font = [Design setFontLiberationSerifBoldItalicWithSize:14];
    _creationDate.textColor=[Design setColor];

    self.backgroundView = [Design setClearBackView];
}


+(AuthorAndDateCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"authorAndDateCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"authorAndDateCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
