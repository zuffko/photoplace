//
//  AddCommentCell.h
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCommentCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UITextField *commentTextField;
@property (nonatomic, retain) IBOutlet UILabel *addCommentCaption;


+(AddCommentCell*)cell;
+(NSString *)cellID;
@end
