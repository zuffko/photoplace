//
//  DescriptionCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "DescriptionCell.h"
#import "Design.h"

@implementation DescriptionCell
@synthesize descriptionTextView=_descriptionTextView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(void)awakeFromNib
{
    self.backgroundView = [Design setClearBackView];
    
    _descriptionTextView=[[UITextView alloc] initWithFrame:CGRectMake(5, 0, 310, 10)];
    _descriptionTextView.font = [Design setFontLiberationSerifItalicWithSize:15];
    _descriptionTextView.backgroundColor = [UIColor clearColor];
    _descriptionTextView.scrollEnabled = NO;
    _descriptionTextView.textColor=[Design setColor];
    _descriptionTextView.editable=NO;
    [self.contentView addSubview:_descriptionTextView];
}


+(DescriptionCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"descriptionCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"descriptionCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
