//
//  Design.m
//  PhotoPlace
//
//  Created by zufa on 17/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "Design.h"

@implementation Design

+(UIFont*)setFontLobsterWithSize:(CGFloat)fontSize
{
    UIFont *lobster = [UIFont fontWithName:@"Lobster" size:(CGFloat)fontSize];
    return lobster;
}

+(UIFont*)setFontLiberationSerifBoldWithSize:(CGFloat)fontSize
{
    UIFont *liberationSerifBold = [UIFont fontWithName:@"LiberationSerif-Bold" size:(CGFloat)fontSize];
    return liberationSerifBold;
}

+(UIFont*)setFontLiberationSerifBoldItalicWithSize:(CGFloat)fontSize
{
    UIFont *liberationSerifBoldItalic = [UIFont fontWithName:@"LiberationSerif-BoldItalic" size:(CGFloat)fontSize];
    return liberationSerifBoldItalic;
}

+(UIFont*)setFontLiberationSerifItalicWithSize:(CGFloat)fontSize
{
    UIFont *liberationSerifItalic = [UIFont fontWithName:@"LiberationSerif-Italic" size:(CGFloat)fontSize];
    return liberationSerifItalic;
}

+(UIFont*)setFontLiberationSerifRegularWithSize:(CGFloat)fontSize
{
    UIFont *liberationSerifRegular = [UIFont fontWithName:@"LiberationSerif-Regular" size:(CGFloat)fontSize];
    return liberationSerifRegular;
}

+(UIColor*)setColor
{
    UIColor *intarfaceElementsColor =[UIColor colorWithRed:(73/255.f) green:(70/255.f) blue:(63/255.f) alpha:1];
    return intarfaceElementsColor;
}

+(UIView*)setClearBackView
{
    UIView *backView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    backView.backgroundColor = [UIColor clearColor];
    return backView;
}
@end
