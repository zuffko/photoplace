//
//  CommentPFObjectSubclass.h
//  PhotoPlace
//
//  Created by zufa on 15/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Parse/Parse.h>

@interface Comment : PFObject <PFSubclassing>
@property (retain) NSString *commentText;
@property (retain) NSString *commentCreateDate;
@property (retain) PFObject *commentBelongsPointer;
@property (retain) NSString *commentAuthorFacebookId;
@property (retain) NSString *commentAuthorDisplayName;

+ (NSString *)parseClassName;
@end
