//
//  Design.h
//  PhotoPlace
//
//  Created by zufa on 17/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Design : NSObject
+(UIFont*)setFontLobsterWithSize:(CGFloat)fontSize;
+(UIFont*)setFontLiberationSerifBoldWithSize:(CGFloat)fontSize;
+(UIFont*)setFontLiberationSerifBoldItalicWithSize:(CGFloat)fontSize;
+(UIFont*)setFontLiberationSerifItalicWithSize:(CGFloat)fontSize;
+(UIFont*)setFontLiberationSerifRegularWithSize:(CGFloat)fontSize;
+(UIColor*)setColor;
+(UIView*)setClearBackView;

@end
