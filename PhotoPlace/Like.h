//
//  LikePFObjectSubclass.h
//  PhotoPlace
//
//  Created by zufa on 15/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Parse/Parse.h>

@interface Like : PFObject <PFSubclassing>
@property (retain) NSString *likeText;
@property (retain) NSString *likeAuthorFacebookId;
@property (retain) PFObject *likePlacePointer;


+ (NSString *)parseClassName;
@end
