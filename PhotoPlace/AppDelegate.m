//
//  AppDelegate.m
//  PhotoPlace
//
//  Created by zufa on 4/4/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "AddPlaceViewController.h"
#import "Place.h"
#import "Comment.h"
#import "Like.h"
#import "User.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Like registerSubclass];
    [Comment registerSubclass];
    [Place registerSubclass];
    [Parse setApplicationId:@"MJKom9rsKGCwC1bm5gOtdSTAVp2QGXzZWxvifm2h"
                  clientKey:@"ryih6IIyN4Sk2YqJOaQJA2lp9GoSCIN8Kciee9GD"];
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [PFFacebookUtils  initializeFacebook ];
    
    UIImage *navBar = [UIImage imageNamed:@"nav_bar.png"];
    
    [[UINavigationBar appearance] setBackgroundImage:navBar forBarMetrics:UIBarMetricsDefault];
    
    UIImage *image = [UIImage imageNamed: @"back.png"];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:image forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    UIImage *barButtonImage = [[UIImage imageNamed:@"bar_button.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    [[UIBarButtonItem appearance] setBackgroundImage:barButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];    return YES;
    
    
}

+ (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title {
    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.labelText = title;
    return hud;
}

+ (void)dismissGlobalHUD {
    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    [MBProgressHUD hideHUDForView:window animated:YES];
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [PFFacebookUtils handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [PFFacebookUtils handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{

}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end
