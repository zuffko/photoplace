
#import "LoginViewController.h"
#import "PlaceTableViewController.h"
#import "Constants.h"
#import "User.h"
#import "Utilites.h"

@interface LoginViewController ()
@property (nonatomic, strong) NSDictionary *userProfile;
@end

@implementation LoginViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"DetailTVC_background.png"]];
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]])
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                        message:@"User with facebook logged in!"
                                                       delegate:self
                                              cancelButtonTitle:@"Back"
                                              otherButtonTitles:@"Logout", nil];
        [alert show];
        [alert release];
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0){
        [self.navigationController popViewControllerAnimated:YES];
    }else if (buttonIndex == 1){
        [PFUser logOut];
    }
}

- (void) awakeFromNib
{
    [_fbLoginButton setBackgroundImage:[UIImage imageNamed:@"login-button-small.png"] forState:UIControlStateApplication];
}

- (IBAction)FBlogin:(id)sender  {
[self.navigationController popViewControllerAnimated:YES];
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];

    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
            }
            
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            [self.navigationController popViewControllerAnimated:YES];
            [self uploadUserData];          
        } else {
            NSLog(@"User with facebook logged in!");
            [self.navigationController popViewControllerAnimated:YES];
            [self uploadUserData];
            
        }
    }];
      }

-(void)uploadUserData
{
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            NSDictionary *userData = (NSDictionary *)result;
            NSLog(@"%@", userData);
            NSString *facebookID = userData[@"id"];
            
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
            NSData *data = [NSData dataWithContentsOfURL:pictureURL];
            UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
            
            CGRect rect=CGRectMake(0,0, 200, 200);
            UIImage *qwe = [Utilites imageCrop:image toRect:rect];
            NSData *imageData = UIImagePNGRepresentation(qwe);
            PFFile *imageFile = [PFFile fileWithData:imageData];
            [imageFile save];

            [PFUser currentUser].userDisplayName=userData[@"name"];
            [PFUser currentUser].userAvatar=imageFile;
            [PFUser currentUser].userFacebookId = userData[@"id"];
            [[PFUser currentUser] saveInBackground];

        } else if ([[[[error userInfo] objectForKey:@"error"] objectForKey:@"type"]
                    isEqualToString: @"OAuthException"]) { 
            NSLog(@"The facebook session was invalidated");
            
        } else {
            NSLog(@"Some other error: %@", error);
        }
    }];

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
