//
//  CommentsCell.h
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UILabel *commentText;
@property (nonatomic, retain) IBOutlet UILabel *commentsAuthorName;
@property (nonatomic, retain) IBOutlet UILabel *commentsDate;
@property (nonatomic, retain) IBOutlet UIImageView *userAvatar;




+(CommentsCell*)cell;
+(NSString *)cellID;
@end
