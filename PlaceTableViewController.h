
#import <UIKit/UIKit.h>
#import "PlaceDetailTableViewController.h"
#import "ProtocolQueries.h"

@interface PlaceTableViewController : UITableViewController <UITableViewDelegate, ProtocolQueries>


@property (nonatomic,retain) NSArray *places;
@property (assign, nonatomic) BOOL ascending;
@property (nonatomic, retain) NSMutableArray *cells;
@property (nonatomic, strong) Place *loadplace;
- (void)reloadData:(id)sender;
@end