//
//  PlasesCell.h
//  PhotoPlace
//
//  Created by zufa on 22/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlasesCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UILabel *placeTitle;
@property (nonatomic, retain) IBOutlet UILabel *placeAuthorName;
@property (nonatomic, retain) IBOutlet UILabel *placeDate;
@property (nonatomic, retain) IBOutlet UIImageView *placePhoto;
+(PlasesCell*)cell;
+(NSString *)cellID;
@end
