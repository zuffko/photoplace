//
//  PlasesCell.m
//  PhotoPlace
//
//  Created by zufa on 22/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "PlasesCell.h"
#import "Design.h"

@implementation PlasesCell
@synthesize placeAuthorName=_placeAuthorName;
@synthesize placeDate=_placeDate;
@synthesize placeTitle=_placeTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    
    
    return self;
}
-(void)awakeFromNib
{
    

    _placeTitle.font=[Design setFontLobsterWithSize:20];
    
    _placeAuthorName.font=[Design setFontLiberationSerifItalicWithSize:16];
    
    _placeDate.font=[Design setFontLiberationSerifItalicWithSize:12];

}

+(PlasesCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"Cell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
