//
//  LikePFObjectSubclass.m
//  PhotoPlace
//
//  Created by zufa on 15/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "Like.h"
#import <Parse/PFObject+Subclass.h>

@implementation Like
@dynamic likeText;
@dynamic likeAuthorFacebookId;
@dynamic likePlacePointer;

+ (NSString *)parseClassName {
    return @"Likes";
}
@end
 