
#import "PlaceTableViewController.h"
#import "Place.h"
#import <Parse/Parse.h>
#import "AddPlaceViewController.h"
#import "Constants.h"
#import "Design.h"
#import "PlasesCell.h"

@interface PlaceTableViewController ()

@end

@implementation PlaceTableViewController
@synthesize places=_places;
@synthesize cells=_cells;
@synthesize loadplace=_loadplace;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(reloadData:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    ProtocolQueries *data =[ProtocolQueries new];
    data.delegate = self;
    [data getPlacesWithCashePolicy:(int)kPFCachePolicyCacheElseNetwork];
    self.tableView.rowHeight = 320;
}

-(void)setPlases:(NSArray*) plases
{
    _places=(NSArray*) plases;
    [_places retain];
    [self setCell];
    [self hideHud];
    [self.tableView reloadData];
}

-(void)setCell
{
    _cells = [[NSMutableArray alloc] init];
    int i=0;
    while (i<[_places count]) {
        
        PlasesCell *cell;
        cell = [self.tableView dequeueReusableCellWithIdentifier:[PlasesCell cellID]];
        if (!cell) {
            cell=[[[PlasesCell cell]retain] autorelease];
        }
        _loadplace = [_places objectAtIndex:i];
        cell.placeTitle.text = _loadplace.placeTitle;
        
        cell.placeAuthorName.text = _loadplace.placeAuthorDisplayName;
        
        cell.placeDate.text = _loadplace.placeCreateDate;
        
        PFFile *file = _loadplace.placePhoto;
        [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
            UIImage *pickImage = [UIImage imageWithData:data];
            cell.placePhoto.image = pickImage;
            }
        }];

        i++;
        [_cells addObject:cell];
    }
}

-(void)ShowHUD
{
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
}

-(void)hideHud
{
    [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
    [self.tableView reloadData];
}

- (void)reloadData:(id)sender
{
    [PFQuery clearAllCachedResults];
//    ProtocolQueries *data =[[ProtocolQueries alloc]init];
//    [data getPlacesWithCashePolicy:kPFCachePolicyNetworkElseCache];
//    _ascending =! _ascending;
    [self viewDidLoad];
    [self.refreshControl endRefreshing];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [_cells objectAtIndex:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"PlaceDetailTableViewController" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PlaceDetailTableViewController"]) {
        PlaceDetailTableViewController *placeDetailTableViewController = segue.destinationViewController;
        placeDetailTableViewController.loadplace=[_places objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    }
}

- (void)dealloc {
    
    [super dealloc];
    [_places release];
}
@end
