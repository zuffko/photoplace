//
//  TitleCell.h
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UITextView *titleTextView;
+(TitleCell*)cell;
+(NSString *)cellID;
@end
