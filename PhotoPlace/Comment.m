//
//  CommentPFObjectSubclass.m
//  PhotoPlace
//
//  Created by zufa on 15/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "Comment.h"
#import <Parse/PFObject+Subclass.h>

@implementation Comment
@dynamic commentText;
@dynamic commentCreateDate;
@dynamic commentAuthorDisplayName;
@dynamic commentAuthorFacebookId;
@dynamic commentBelongsPointer;

+ (NSString *)parseClassName {
    return @"Comments";
}
@end
