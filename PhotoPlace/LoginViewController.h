
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface LoginViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic, retain) IBOutlet UIButton *fbLoginButton;

-(void)uploadUserData;
@end
