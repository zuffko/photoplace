//
//  PlacePFObjectSubclass.m
//  PhotoPlace
//
//  Created by zufa on 15/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "Place.h"
#import <Parse/PFObject+Subclass.h>

@implementation Place
@dynamic placeTitle;
@dynamic placeDescription;
@dynamic placAauthorPoiter;
@dynamic placeCreateDate;
@dynamic placeAuthorDisplayName;
@dynamic placePhoto;
@dynamic placeLatitude;
@dynamic placeLongitude;

+ (NSString *)parseClassName {
    return @"Place";
}

@end
