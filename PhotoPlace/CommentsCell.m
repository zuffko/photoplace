//
//  CommentsCell.m
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "CommentsCell.h"
#import "Design.h"

@implementation CommentsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(void)awakeFromNib
{
    _commentsAuthorName.font = [Design setFontLobsterWithSize:15];
    _commentsAuthorName.textColor=[Design setColor];
    
    _commentText.font = [Design setFontLiberationSerifItalicWithSize:15];
    _commentText.textColor=[Design setColor];
    
    _commentsDate.font = [Design setFontLiberationSerifItalicWithSize:10];
    _commentsDate.textColor=[Design setColor];

    self.backgroundView = [Design setClearBackView];

}


+(CommentsCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"commentCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"commentCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
