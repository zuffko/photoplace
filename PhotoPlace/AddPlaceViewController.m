
#import "AddPlaceViewController.h"
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Place.h"
#import "LoginViewController.h"
#import "Constants.h"
#import "Place.h"

#define METERS_PER_MILE 1609.344
@interface AddPlaceViewController (){
    UIImage *pickedImage;
    CLLocationManager *locationManager;
}


@end

@implementation AddPlaceViewController
@synthesize map;
@synthesize gps;
@synthesize mapShow=_mapShow;

static NSString *latitude;
static NSString *longitude;


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _mapShow.delegate = self;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"DetailTVC_background.png"]];

    self.placeTitleTextField.delegate= self;
    self.placeDescriptionTextField.delegate = self;
    self.navigationItem.title = @"Add place";

    if (![PFUser currentUser] && ![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]])
    {
        UIAlertView* alertLogin = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                        message:@"You not logged in! :("
                                                       delegate:self
                                              cancelButtonTitle:@"login"
                                              otherButtonTitles:@"Cancel", nil];
        [alertLogin show];
        [alertLogin release];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 39.281516;
    zoomLocation.longitude= -76.580806;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    
    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake(49, -123);
    MKCoordinateRegion adjustedRegion = [_mapShow regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 200, 200)];
    [_mapShow setRegion:adjustedRegion animated:YES];
    // 3
    [_mapShow setRegion:viewRegion animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

        if (buttonIndex == 0){
            [self performSegueWithIdentifier:@"ShowDetail" sender:nil];
        }else if (buttonIndex == 1){
            [self.navigationController popViewControllerAnimated:YES];
        }
}

- (IBAction)addImage:(id)sender {
    UIActionSheet *action = [[[UIActionSheet alloc] initWithTitle:@"Select image from"
                                                         delegate:self cancelButtonTitle:@"Cancel"
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:@"From camera",@"From library", nil] autorelease];
    [self.view endEditing:YES];
    [action showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 ) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *pickerView =[[UIImagePickerController alloc]init];
            pickerView.allowsEditing = YES;
            pickerView.delegate = self;
            pickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pickerView animated:YES completion:nil];
        }
        
    }
    else if( buttonIndex == 1 )
    {
        
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pickerController.allowsEditing = YES;
            pickerController.delegate = self;
            
            [self presentViewController:pickerController animated:YES completion:NULL];
        }
        [pickerController autorelease];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (image == nil)
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    pickedImage=image;
    [self.imageView setImage:image];
}

- (IBAction) addPlace:(id)sender;{
    
    [self.view endEditing:YES];
    if ([_placeTitleTextField.text length]==0 || [_placeDescriptionTextField.text length]==0) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Add place"
                                                        message:@"You have not filled all fields!"
                                                       delegate:nil
                                              cancelButtonTitle:@"I fill"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];

    }else{
        NSDate *dateToday =[NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeStyle:NSDateFormatterShortStyle];
        [format setDateStyle:NSDateFormatterMediumStyle];
        NSString *string = [format stringFromDate:dateToday];
        [format release];
        NSData *imageData = UIImagePNGRepresentation(pickedImage);
        PFFile *imageFile = [PFFile fileWithData:imageData];

        Place *currentPlace = [Place object];
        currentPlace.placeTitle=_placeTitleTextField.text;
        currentPlace.placeDescription= _placeDescriptionTextField.text;
        currentPlace.placeAuthorDisplayName= [[PFUser currentUser] objectForKey:userDisplayNameKey];
        currentPlace.placeCreateDate= string;
        currentPlace.placeLatitude= latitude;
        currentPlace.placeLongitude= longitude;
        currentPlace.placePhoto = imageFile;
        currentPlace.placAauthorPoiter = [PFUser currentUser];
        [currentPlace saveInBackground];
    }
}

-(IBAction)setLocation:(id)sender;
{
    map.showsUserLocation=YES;
    [map setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];

}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *loc = [locations lastObject];
    [locationManager stopUpdatingLocation];
    latitude = [[NSString alloc] initWithFormat:@"%f", loc.coordinate.latitude];
    longitude = [[NSString alloc] initWithFormat:@"%f", loc.coordinate.longitude];
}

- (void)didReceiveMemoryWarning1
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [super dealloc];
    [map release];
    [gps release];
    [pickedImage release];
    [locationManager release];
}



@end

