
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocation.h>

@interface AddPlaceViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, CLLocationManagerDelegate, MKMapViewDelegate>
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic,retain) IBOutlet UITextField *placeTitleTextField;
@property (nonatomic,retain) IBOutlet UITextField *placeDescriptionTextField;
@property (nonatomic, retain) IBOutlet MKMapView *map;
@property (nonatomic, retain) IBOutlet MKMapView *mapShow;
@property (nonatomic, retain) CLLocationManager *gps;
-(IBAction)setLocation :(id)sender;
@end
