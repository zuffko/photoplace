//
//  AuthorAndDateCell.h
//  PhotoPlace
//
//  Created by zufa on 11/5/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AuthorAndDateCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UILabel *authorName;
@property (nonatomic, retain) IBOutlet UILabel *creationDate;

+(AuthorAndDateCell*)cell;
+(NSString *)cellID;
@end
